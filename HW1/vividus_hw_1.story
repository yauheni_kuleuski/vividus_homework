
Scenario: Create account

Given I am on a page with the URL '$signUpPageURL' 
When I enter '$firstName' in a field by the xpath '//form[@id="sign-up-form"]/div/input[@id="first-name-su"]'
When I enter '$lastName' in a field by the xpath '//form[@id="sign-up-form"]/div/input[@id="last-name-su"]'
When I enter '$email' in a field by the xpath '//form[@id="sign-up-form"]/div/input[@id="email-su"]'
When I enter '$password' in a field by the xpath '//form[@id="sign-up-form"]/div/input[@id="email-su"]'
When I click on an element by the xpath '//form[@id="sign-up-form"]/button[text()="Create account"]'
Then an element by the xpath '//span[@id="recaptcha-email"]' exists


Scenario: Perform search for product by search box 

Given I am on a page with the URL '$homePageURL'
When I enter '$productName' in a field by the xpath '//input[@id="global-search-input"]'
When I click on an element by the xpath '//button[@id="global-search-submit"]'
Then an element by the xpath '//div[@id="searchProductResult"]/h1' exists


Scenario: Open product page for product from previous step  

When I click on an element by the xpath '//div[@class="search-result-listview-items"]/div[1]//div[contains(@class, "search-result-product-title")]/a'
Then an element by the xpath '//div[@id="product-overview"]' exists


Scenario: Add product to cart

When I click on an element by the xpath '//button[contains(@class, "prod-ProductCTA--primary")]'
Then an element by the xpath '//span[contains(*, "You just added 1 item")]' exists


Scenario: Navigate to cart

Given I am on a page with the URL '$homePageURL'
When I click on an element by the xpath '//a[@id="hf-cart"]'
Then an element by the xpath '//span[contains(*, "Your cart: ")]' exists
Then an element by the xpath '//div[contains(@class, "cart-list")]//div[@class="cart-item-row"]' exists
